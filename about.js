window.addEventListener('load', function (e) {
	if (navigator.serviceWorker) {
		navigator.serviceWorker.ready.then(function (reg) {
			reg.active.postMessage({
				type: 'swVersionRequest',
			})
		})
		navigator.serviceWorker.addEventListener('message', function (e) {
			if ('type' in e.data) {
				switch (e.data.type) {
					case 'swVersion': {
						document.getElementById('sw-version-div').classList.remove('hidden')
						document.getElementById('sw-version').textContent = e.data.version
					}
				}
			}
		})
	}
})
