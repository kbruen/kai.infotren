/**
 * Create <span class="{IR|IC|}">IR|IRN|IC|R|R-E</span> <span>74</span> inside element
 * @param {string} rank
 * @param {string} number
 * @param {HTMLElement|undefined} element
 * @returns {HTMLSpanElement|HTMLElement}
 */
function trainIdSpan(rank, number, element) {
	if (!element) {
		element = document.createElement('span')
	}

	var rankSpan = document.createElement('span')
	element.appendChild(rankSpan)
	rankSpan.textContent = rank
	if (rank.startsWith('IC')) {
		rankSpan.classList.add('IC')
	}
	else if (rank.startsWith('IR')) {
		rankSpan.classList.add('IR')
	}

	element.appendChild(document.createTextNode(' '))

	var numberSpan = document.createElement('span')
	element.appendChild(numberSpan)
	numberSpan.textContent = number

	return element
}
