window.addEventListener('load', function (e) {
	document.body.addEventListener('keydown', function (e) {
		if (e.key == 'Backspace' && (e.target.tagName.toUpperCase() !== 'INPUT' || e.target.value.length === 0)) {
			e.preventDefault()
			window.history.back()
		}
	})

	var backButton = document.getElementById('back-button')
	if (backButton) {
		backButton.addEventListener('click', function (e) {
			window.history.back()
		})
	}
})
