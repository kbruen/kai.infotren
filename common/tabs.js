var selectedTab = 0

/**
 * @param {number} idx
 */
function selectTab(idx) {
	var tabs = document.querySelectorAll('.tabs h3')
	var tabViews = document.querySelectorAll('.tab-view')
	if (tabs.length > 0) {
		tabs.forEach(function (tab, tIdx) {
			if (idx == tIdx) {
				return
			}

			tab.classList.remove('selected')
			tabViews[tIdx].classList.add('hidden')
		})
		tabs[idx].classList.add('selected')
		tabViews[idx].classList.remove('hidden')
	}
}

/**
 * @param {number} offset
 */
function tabNav(offset) {
	var items = document.querySelectorAll('.tab-view')

	selectedTab += offset
	if (selectedTab < 0) {
		selectedTab += items.length
	}
	if (selectedTab >= items.length) {
		selectedTab -= items.length
	}
	selectTab(selectedTab)
}

function handleTabKeyDown(e) {
	switch (e.key) {
		case 'ArrowLeft':
			e.preventDefault()
			e.stopPropagation()
			tabNav(-1)
			break
		case 'ArrowRight':
			e.preventDefault()
			e.stopPropagation()
			tabNav(1)
			break
	}
}

window.addEventListener('load', function (e) {
	// Select first item
	selectTab(selectedTab)
	// Add onclick
	document.querySelectorAll('.tabs h3').forEach(function (tab, tIdx) {
		tab.addEventListener('click', function (e) {
			selectedTab = tIdx
			selectTab(selectedTab)
		})
	})

	document.body.addEventListener('keydown', handleTabKeyDown)
})
