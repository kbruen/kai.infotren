const VERSION = 'v36'
const API_ORIGIN = 'https://scraper.infotren.dcdev.ro/'
const API_TRAINS = `${API_ORIGIN}v3/trains`
const API_STATIONS = `${API_ORIGIN}v3/stations`
const API_ITINERARIES = `${API_ORIGIN}v3/itineraries`

const CACHE_FIRST = [
	// Root
	'/',

	// Static assets
	'/common/back.svg',

	// Utility JS
	'/common/worker.js',
	'/common/items.js',
	'/common/back.js',
	'/common/tabs.js',
	'/common/trainId.js',

	// Base
	'/base.css',
	'/base.dark.css',

	// Pages
	'/index.html',
	'/index.js',

	'/about.html',
	'/about.js',

	'/train.html',
	'/train.js',

	'/view-train.html',
	'/view-train.js',
	'/view-train.css',
	'/view-train.dark.css',

	'/station.html',
	'/station.js',

	'/view-station.html',
	'/view-station.js',
	'/view-station.css',
	'/view-station.dark.css',

	'/route.html',
	'/route.js',
	'/route.css',

	// API
	API_TRAINS,
	API_STATIONS,
	// API_ITINERARIES,
];

/**
 * @param {string} url
 * @returns {boolean}
 */
function shouldReturnFromCacheFirst(url) {
	return CACHE_FIRST.map(u => u.includes('://') ? u : self.location.origin + u).includes(url.split('?')[0])
}

self.addEventListener('install', (event) => {
	if ('skipInstall' in self) {
		self.skipInstall()
	}
	event.waitUntil(
		caches
			.open(VERSION)
			.then((cache) =>
				cache.addAll(CACHE_FIRST)
			)
	)
})

const deleteCache = (/** @type {string} */ key) => caches.delete(key)

const deleteOldCaches = async () => {
	const cacheKeepList = [VERSION]
	const keyList = await caches.keys()
	const cachesToDelete = keyList.filter((key) => !cacheKeepList.includes(key))
	await Promise.all(cachesToDelete.map(deleteCache))
}

// Enable navigation preload
const enableNavigationPreload = async () => {
	if (self.registration.navigationPreload) {
		// Enable navigation preloads!
		await self.registration.navigationPreload.enable()
	}
}

self.addEventListener('activate', (event) => {
	event.waitUntil(Promise.all([deleteOldCaches(), enableNavigationPreload()]))
})

self.addEventListener('message', (event) => {
	if ('type' in event.data) {
		switch (event.data.type) {
			case 'swVersionRequest': {
				event.source.postMessage({
					type: 'swVersion',
					version: VERSION,
				})
				break;
			}
		}
	}
})

/**
 * @param {RequestInfo | URL} request
 * @param {Response} response
 * @returns {Promise<void>}
 */
const putInCache = async (request, response) => {
	const cache = await caches.open(VERSION)
	// try {
	// 	response.headers.set('SW-Cached-At', new Date().toISOString())
	// }
	// catch {
		const headers = new Headers(response.headers)
		headers.set('SW-Cached-At', new Date().toISOString())
		response = new Response(response.body, {
			status: response.status,
			statusText: response.statusText,
			headers,
		})
	// }
	await cache.put(request, response)
}

const cacheFirst = async ({ request, preloadResponsePromise, refreshAnyway }) => {
	// First try to get the resource from the cache
	const responseFromCache = await caches.match(request)
	if (responseFromCache) {
		if (refreshAnyway || (responseFromCache.headers.has('SW-Cached-At') && Date.now() - new Date(responseFromCache.headers.get('SW-Cached-At')).valueOf() > 86400000)) {
			console.log('[cf] using cache response; refreshing anyway but returning cache', responseFromCache);
			((async () => {
				try {
					const response = await fetch(request)
					if (response.ok) {
						await putInCache(request, response)
					}
				}
				catch {}
			})())
		}
		else {
			console.log('[cf] using cache response', responseFromCache)
		}
		return responseFromCache
	}

	// Next try to use (and cache) the preloaded response, if it's there
	try {
		if (preloadResponsePromise) {
			const preloadResponse = await preloadResponsePromise
			if (preloadResponse && preloadResponse.ok) {
				console.info('[cf] using preload response', preloadResponse)
				await putInCache(request, preloadResponse.clone())
				return preloadResponse
			}
			else {
				console.log('[cf] got not ok preloadResponse, ignoring', preloadResponse)
			}
		}
	}
	catch (e) {
		// Ignore as preload isn't necessarily important
		console.error('[cf] preload response error', e)
	}

	// Next try to get the resource from the network
	const responseFromNetwork = await fetch(request)
	// response may be used only once
	// we need to save clone to put one copy in cache
	// and serve second one
	if (responseFromNetwork.ok) {
		console.log('[cf] using network response; ok so also cache', responseFromNetwork)
		await putInCache(request, responseFromNetwork.clone())
	}
	else {
		console.log('[cf] using network not ok response', responseFromNetwork)
	}
	return responseFromNetwork;
}

const networkFirst = async ({ request, preloadResponsePromise }) => {
	// First try to use (and cache) the preloaded response, if it's there
	try {
		if (preloadResponsePromise) {
			const preloadResponse = await preloadResponsePromise
			if (preloadResponse && preloadResponse.ok) {
				console.info('[nf] using preload response', preloadResponse)
				await putInCache(request, preloadResponse.clone())
				return preloadResponse
			}
			else {
				console.log('[nf] got not ok preloadResponse, ignoring', preloadResponse)
			}
		}
	}
	catch (e) {
		// Ignore as preload isn't necessarily important
		console.error('[nf] preload response error', e)
	}

	// Next try to get the resource from the network
	let responseFromNetwork
	let errorFromNetwork
	try {
		responseFromNetwork = await fetch(request)
	}
	catch (e) {
		responseFromNetwork = null
		errorFromNetwork = e
	}
	// If the response is ok, put it in cache and respond
	if (responseFromNetwork && responseFromNetwork.ok) {
		console.log('[nf] using ok network response', responseFromNetwork)
		await putInCache(request, responseFromNetwork.clone())
		return responseFromNetwork
	}

	// Response from network wasn't ok, try to find in cache
	const responseFromCache = await caches.match(request)
	if (responseFromCache) {
		console.log('[nf] using cache response', responseFromCache)
		return responseFromCache
	}

	// If we didn't find a cached response, return the fresh network error
	if (responseFromNetwork) {
		console.log('[nf] using network not ok response', responseFromNetwork)
		return responseFromNetwork
	}
	else {
		console.log('[nf] throwing network error', errorFromNetwork)
		throw errorFromNetwork
	}
}

self.addEventListener('fetch', (event) => {
	if (shouldReturnFromCacheFirst(event.request.url)) {
		event.respondWith(
			cacheFirst({
				request: event.request,
				preloadResponsePromise: event.preloadResponse,
				refreshAnyway: [API_STATIONS, API_TRAINS].includes(event.request.url.split('?')[0]),
			})
		)
	}
	else {
		event.respondWith(
			networkFirst({
				request: event.request,
				preloadResponsePromise: event.preloadResponse,
			})
		)
	}
})
