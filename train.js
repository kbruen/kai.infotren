var knownTrains = []

function goToTrain(number) {
	var url = new URL(window.location.href)
	url.pathname = 'view-train.html'
	url.searchParams.set('train', number)
	url.searchParams.set('date', new Date().toISOString())
	window.location.href = url.toString()
}

var focusedElement = null

var _rebuildDebounce = null
var _rebuildRequested = false
function rebuildSuggestions() {
	if (_rebuildDebounce !== null) {
		_rebuildRequested = true
		return
	}
	
	_rebuildRequested = false
	_rebuildDebounce = 123

	var suggestionsArea = document.getElementById('suggestionsArea')
	while (suggestionsArea.childNodes.length > 0) {
		suggestionsArea.childNodes[0].remove()
	}

	var trainNumberInput = document.getElementById('trainNumber')
	var trainNumber = trainNumberInput.value.trim()

	var suggestions = []
	if (!trainNumber) {
		suggestions = knownTrains.slice()
	}
	else {
		for (var i = 0; i < knownTrains.length; i++) {
			if (!knownTrains[i].number.includes(trainNumber)) {
				continue
			}
			suggestions.push(knownTrains[i])
		}
		suggestions.sort((s1, s2) => {
			if (s1.number.indexOf(trainNumber) != s2.number.indexOf(trainNumber)) {
				return s1.number.indexOf(trainNumber) - s2.number.indexOf(trainNumber);
			}

			if (s1.number.length != s2.number.length) {
				return s1.number.length - s2.number.length;
			}

			return s1.number.localeCompare(s2.number);
		})
	}

	// Trim the amount of results displayed
	if (suggestions.length > 100) {
		suggestions.splice(100)
	}

	var foundInput = false
	suggestions.forEach(function (suggestion, index) {
		if (trainNumber == suggestion.number) {
			foundInput = true
		}
		var suggestionLi = document.createElement('li')
		suggestionsArea.appendChild(suggestionLi)

		setTimeout(function () {
			suggestionLi.classList.add('items')
			suggestionLi.tabIndex = index + 1
			suggestionLi.style.padding = '2px 0'

			function onAction(e) {
				goToTrain(suggestion.number)
			}
			suggestionLi.addEventListener('click', onAction)
			suggestionLi.addEventListener('keypress', function (e) {
				if (e.key == 'Enter') {
					onAction(e)
				}
			})
			suggestionLi.addEventListener('focus', function (e) {
				focusedElement = suggestionLi
			})

			var trainNameP = document.createElement('p')
			suggestionLi.appendChild(trainNameP)

			trainIdSpan(suggestion.rank, suggestion.number, trainNameP)
			trainNameP.classList.add('pri', 'trainName')

			var trainCompanyP = document.createElement('p')
			suggestionLi.appendChild(trainCompanyP)

			trainCompanyP.textContent = suggestion.company
			trainCompanyP.classList.add('thi')
		}, 0)
	})
	if (!foundInput && trainNumber) {
		var suggestionLi = document.createElement('li')
		suggestionsArea.appendChild(suggestionLi)

		suggestionLi.classList.add('items')
		suggestionLi.tabIndex = suggestions.length + 2
		suggestionLi.style.padding = '2px 0'

		function onAction(e) {
			goToTrain(trainNumber)
		}
		suggestionLi.addEventListener('click', onAction)
		suggestionLi.addEventListener('keypress', function (e) {
			if (e.key == 'Enter') {
				onAction(e)
			}
		})
		suggestionLi.addEventListener('focus', function (e) {
			focusedElement = suggestionLi
		})

		var trainNameP = document.createElement('p')
		suggestionLi.appendChild(trainNameP)

		trainNameP.textContent = `Train ${trainNumber}`
		trainNameP.classList.add('pri', 'trainName')
	}

	setTimeout(function () {
		_rebuildDebounce = null
		if (_rebuildRequested) {
			rebuildSuggestions()
		}
	}, 500)
}

function lsk() {
	document.getElementById('trainNumber').focus()
}

function csk() {
	if (focusedElement == null) {
		return
	}

	if (focusedElement.id === 'trainNumber') {
		goToTrain(document.activeElement.value.trim())
	}
	else {
		focusedElement.click()
	}
}

window.addEventListener('load', function (e) {
	var trainNumber = document.getElementById('trainNumber')
	trainNumber.addEventListener('input', function (e) {
		rebuildSuggestions()
	})
	trainNumber.addEventListener('focus', function (e) {
		focusedElement = trainNumber
		document.getElementsByClassName('lsk')[0].textContent = ''
		document.getElementsByClassName('csk')[0].textContent = 'Search'
	})
	trainNumber.addEventListener('blur', function (e) {
		document.getElementsByClassName('lsk')[0].textContent = 'Search'
		document.getElementsByClassName('csk')[0].textContent = 'Select'
	})
	trainNumber.addEventListener('keypress', function (e) {
		if (e.key == 'Enter') {
			goToTrain(trainNumber.value.trim())
		}
	})

	document.querySelectorAll('.lsk').forEach(function (lskElem) {
		lskElem.addEventListener('click', function (e) {
			lsk()
		})
	})
	document.querySelectorAll('.csk').forEach(function (cskElem) {
		cskElem.addEventListener('click', function (e) {
			csk()
		})
	})
	document.body.addEventListener('keydown', function (e) {
		if (e.key == 'SoftLeft') {
			lsk()
		}
		else if (e.key == 'Enter') {
			csk()
		}
	})

	fetch('https://scraper.infotren.dcdev.ro/v2/trains')
		.then(function (response) {
			return response.json()
		})
		.then(function (response) {
			knownTrains = response
			knownTrains.sort(function(a, b) { return a.number - b.number })
		})
		.then(function () {
			rebuildSuggestions()
		})
})
