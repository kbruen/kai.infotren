window.addEventListener('load', function (e) {
    if (window.localStorage) {
        var recentViewTrain = localStorage.getItem('recent/view-train')
        if (recentViewTrain) {
            /**
             * @property {string} trainNumber
             * @property {string} date
             * @property {string} $addDate
             * @property {string | undefined} groupIndex
             */
            recentViewTrain = JSON.parse(recentViewTrain)
            var addDate = new Date(recentViewTrain.$addDate)
            addDate.setHours(addDate.getHours() + 2) // store recents for 2 hours
            if (addDate.getTime() > Date.now()) {
                var recentViewTrainLi = document.createElement('li')
                var recentViewTrainLink = document.createElement('a')
                recentViewTrainLi.appendChild(recentViewTrainLink)
                var recentViewTrainLinkUrl = new URL('/view-train.html', window.location.origin)
                recentViewTrainLinkUrl.searchParams.append('train', recentViewTrain.trainNumber)
                recentViewTrainLinkUrl.searchParams.append('date', recentViewTrain.date)
                if (recentViewTrain.groupIndex) {
                    recentViewTrainLinkUrl.searchParams.append('groupIndex', recentViewTrain.groupIndex)
                }
                recentViewTrainLink.href = recentViewTrainLinkUrl.toString()
                recentViewTrainLink.classList.add('items')
                recentViewTrainLink.innerText = `Recent train: ${recentViewTrain.trainNumber}`

                fetch(`https://scraper.infotren.dcdev.ro/v3/trains/${recentViewTrain.trainNumber}?date=${recentViewTrain.date}`)
                    .then(function (result) {
                        if (result.ok) {
                            return result.json()
                        }
                    })
                    .then(function (result) {
                        recentViewTrainLink.innerText = 'Recent train: '
                        trainIdSpan(result.rank, result.number, recentViewTrainLink)
                        if (recentViewTrain.groupIndex !== undefined || result.groups.length === 1) {
                            var group = result.groups[recentViewTrain.groupIndex || 0]
                            if (group.status) {
                                if (group.status.delay === 0) {
                                    recentViewTrainLink.appendChild(document.createTextNode(" (on time)"))
                                }
                                else if (group.status.delay > 0) {
                                    recentViewTrainLink.appendChild(document.createTextNode(` (${group.status.delay} min late)`))
                                }
                                else {
                                    recentViewTrainLink.appendChild(document.createTextNode(` (${-group.status.delay} min early)`))
                                }
                            }
                        }
                    })

                var myTrainLi = document.getElementById("my-train-li")
                myTrainLi.parentNode.insertBefore(recentViewTrainLi, myTrainLi.nextSibling)
            }
        }
    }
})

window.addEventListener('beforeinstallprompt', function (e) {
    var installAppLi = document.createElement('li')
    var installAppLink = document.createElement('a')
    installAppLi.appendChild(installAppLink)
    installAppLink.href = '#'
    installAppLink.classList.add('items')
    installAppLink.innerText = 'Install application'
    installAppLink.addEventListener('click', function (clickE) {
        e.prompt().then(function (_) {
            installAppLi.remove()
        })
    })

    var routesLi = document.getElementById("routes-li")
    routesLi.parentNode.insertBefore(installAppLi, routesLi)
})
